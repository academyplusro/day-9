package service;

import bean.CustomerPackage;

public class CustomerService {
	CustomerPackage customerPackage;

	//setter injection
	public void setCustomerPackage(CustomerPackage customerPackage) {
		this.customerPackage = customerPackage;
	}

	public String callSupport() {
		return customerPackage.support();
	}
}
