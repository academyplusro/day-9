package springIoC;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.context.support.FileSystemXmlApplicationContext;

import service.CustomerService;

@SpringBootApplication
public class SpringIoCApplication {

	public static void main(String[] args) {
		 ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
//		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("E:/STS/WorkPlace/SpringIoC/src/main/resources/bean.xml");

		CustomerService service = context.getBean(CustomerService.class);

		System.out.println(service.callSupport());

		context.close();
	}
}
