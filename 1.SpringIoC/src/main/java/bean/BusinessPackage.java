package bean;

public class BusinessPackage implements CustomerPackage {

	@Override
	public String support() {
		return "Support Business Customers";
	}
}
