package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

	private List<Topic> topics = Arrays.asList(
			new Topic("spring", " spring framework ", " spring framework description"),
			new Topic("c language", " c intermediate ", " c description"),
			new Topic("java", " javascript ",  "java description")
			);
	
	public List<Topic> getAllTopics(){
		return topics;
	}
}
