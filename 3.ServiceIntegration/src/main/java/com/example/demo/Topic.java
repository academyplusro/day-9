package com.example.demo;

public class Topic {

	private String name;
	private String level;
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Topic(String name, String level, String description) {
		super();
		this.name = name;
		this.level = level;
		this.description = description;
	}
	

	
}
