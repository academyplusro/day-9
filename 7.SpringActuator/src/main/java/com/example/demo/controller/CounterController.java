package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CounterController {
	
	@Autowired
	private CounterService counterService; 
	
	@RequestMapping(value="/decrement")
	@ResponseBody
	public String decrement() {
		counterService.increment("important.counter");
		return "Decremented";
	}
	
	@RequestMapping(value="/increment")
	@ResponseBody
	public String increment() {
		counterService.increment("important.counter");
		return "Incremented";
	}

}
