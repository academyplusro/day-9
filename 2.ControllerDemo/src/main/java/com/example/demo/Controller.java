package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping("/hello")
	public String get() {
		return "Hello World!";
	}
	
	@RequestMapping("/welcome")
	public String get1() {
		return "Welcome User";
	}
}
